# Define base image
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

# Copy project files
WORKDIR /app
COPY ["pokemon.csproj", "./"]

# Restore
RUN dotnet restore "./pokemon.csproj"

# Copy all source code
COPY . .

# Publish
WORKDIR .
RUN dotnet publish pokemon.csproj -c Release -o /publish

# Runtime
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "pokemon.dll"]