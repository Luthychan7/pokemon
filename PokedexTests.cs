using Xunit;
using Xunit.Abstractions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Reflection;

namespace pokemon
{
    public class PokedexTests
    {
        private readonly ChromeDriver driver;

        public PokedexTests()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            chromeOptions.AddArgument("disable-gpu");
            chromeOptions.AddArgument("no-sandbox");
            driver = new ChromeDriver(chromeOptions);
        }

        [Fact]
        public void ClickPokedex()
        {
            // Launch Pokemon Website in a Chrome browser
            driver.Navigate().GoToUrl("https://www.pokemon.com/us/");

            // Click "Pokedex" link
            By pokedexLink = By.LinkText("Pokédex");
            driver.FindElement(pokedexLink).Click();

            // Assert on the title of the new page
            By pokedexHeader = By.CssSelector("h1");
            var pokedexHeaderText = driver.FindElement(pokedexHeader).Text;

            Assert.Equal("Pokédex", pokedexHeaderText);

            // Close the browser
            driver.Quit();
        }
    }
}
