# Pokemon! API automation with Postman and GitLab

This is a project I created to learn API test automation using Postman and GitLab, with the aim of having a suite running on demand via GitLab pipelines.

I've tested an API that allows to add Pokemon to a collection. Nice and simple, just the name of the Pokemon and the Id. More on this in the next few sections.

You can see an example report of a test run [here](https://luthychan7.gitlab.io/-/pokemon/-/jobs/406151608/artifacts/report.html).

## What's covered in the suite

The API contains a resource called `pokemon`, with id routing where applicable (i.e: `pokemon/{id}` for `GET`, `PUT` and `DELETE`), plus a JSON request body where the Pokemon name is required (`POST` and `PUT`), following a RESTful principle.

My suite is a collection with four folders, using the CRUD model that maps API verbs to operations nicely. I'm covering positive and negative scenarios alike, validating user imput and business rules. The structure of `PUT` and `DELETE` are derived from `GET` and `POST`, as you'll see.

The resulting structure for the `GET` scenarios contains two positive cases and two negative ones:

### Retrieve Pokemon (`GET`)

- Without providing an `id` (gets all Pokemon in the collection)
- Providing a valid `id` (gets a specific Pokemon)
- Providing a non existent `id`
- Providing an invalid `id`

There are a few specific business rules to cover in order to successfully add a new Pokemon, leaving just the first scenario as a positive case:

### Add a new Pokemon (`POST`)

- Create a new Pokemon providing a valid `name`
- Providing a `name` more than 20 characters long (business rule)
- Providing an empty string as `name` (business rule)
- Providing only spaces as `name` (business rule)
- Providing the `name` of a Pokemon that already exists (business rule)

### Updating a Pokemon (`PUT`)

Updating the name of an existing Pokemon looks fairly similar to a `POST` request in the outlook, except we now add the routing element, which again leaves us with only one positive case:

- Update an existing Pokemon providing a valid `name` and `id`
- Provide a valid `name`, but a non existing `id`
- Provide a valid `name`, but an invalid `id`
- Provide a valid `id`, but a `name` more than 20 characters long
- Provide a valid `id`, but an empty string as `name`
- Provide a valid `id`, but only spaces as `name`
- Provide a valid `id`, but the `name` of a Pokemon that already exists

### Deleting a Pokemon (`DELETE`)

This one only requires the routing element that we already wrote tests for. Only one positive case as before:

- Delete a Pokemon by providing a valid `id`
- Providing a non existent `id`
- Providing an invalid `id`

## Test data

I wanted the suite to re-runnable, and not dependant on existing data, so I've used the `POST` and `DELETE` endpoints to create additional requests prepended with `[Precondition]` and `[Cleanup]` tags, so that I can differentiate them from the tests themselves.

These still contain assertions on response codes, as I want to know whether the report if a test failed due to an actual problem with the system under test, or a precondition that hasn't been satisfied, for example.

In order to pass data between tests, I've used [collection variables](https://learning.getpostman.com/docs/postman/variables-and-environments/variables/) in Postman.

The most common operations in my suite are:

Extracting a created/updated `pokemon_id` as a variable:

```javascript
pm.collectionVariables.set("createdPokemonId", pm.response.json().id);
```

That can be used in a later request to be returned, modified or deleted (in a cleanup step for example).

Extracting a created/updated `pokemon_name` as a variable:

```javascript
pm.collectionVariables.set("createdPokemonName",
    JSON.parse(pm.request.body.raw).Name);
```

That one required parsing the raw request body as JSON, as otherwise the `Name` property wouldn't be accessible as an object.

I opted to use random strings to generate Pokemon names instead of hardcoded values, hence the need to extract it dynamically.

## Assertions

Since all the heavy lifting was done by carefully setting up the requests, dividing the concerns of the suite into multiple tests and linking preconditions with tests and cleanup steps, the only type of assertion needed at this point is on the returned HTTP status code.

The Pokemon API is fairly granular with the response codes, so instead of using ranges, I could expect more specific status codes:

- `201` for successfully created records
- `200` for successuful operations
- `404` for records not found for the provided `id`
- `400` for invalid requests (either bad data or violating business rules)

An example of an assertion for a `200` goes:

```javascript
let expectedStatusCode = 200;

pm.test(`Status code is ${expectedStatusCode}`,
        function()
        {
            pm.response.to.have.status(expectedStatusCode);
        };)
```

Note that I extracted the actual status code as a variable, so that I can reuse the value within the script and only change it in one place.

## Continous integration in GitLab

GitLab allows using pipelines to run tests in a continous integration manner. I based my pipeline configuration on this [excellent tutorial](https://www.youtube.com/watch?v=H0WiDqhDIOs&t=691s) by Valentin Despa.

All we need in GitLab for a pipeline to work is a `gitlab-ci.yml` file with the definition of the pipeline. In our case, the pipeline only has one stage (i.e: run the postman tests) because we are testing against an already hosted app.

GitLab pulls our code (which contains the Postman collection and Environment as json files) and uses a Docker image of Postman called [Newman](https://hub.docker.com/r/postman/newman/). Using that image, we can call the execution as we'd do from the command line:

```
newman run pokemon_tests.json -e pokemon_environment.json --reporters cli,htmlextra --reporter-htmlextra-export report.html
```

<<<<<<< HEAD
Note that we are passing in two types of report: `cli`, for the command line output that we can see in the output of the run, and `htmlextra`, that produces a beautiful html report (see the [project documentation](https://github.com/DannyDainton/newman-reporter-htmlextra)).
=======
Note that we are passing in two types of report: `cli`, for the command line output that we can see in the output of the run, and `htmlextra`, that produces a beautiful html report.
>>>>>>> e2a39778f7d68280d4d88ff98624276e32cb8b5e

For `htmlextra` to work, we need to import it first via npm (as we're using a Linux container), and specify the path to our published paths.

The end result looks quite tidy:

```yml
stages:
- test

postman_tests: 
  stage: test
  when: always
  image:
    name: postman/newman_alpine33
    entrypoint: [""]
  script:
    - newman --version
    - npm install -g newman-reporter-htmlextra
    - newman run pokemon_tests.json -e pokemon_environment.json --reporters cli,htmlextra --reporter-htmlextra-export report.html
  artifacts:
    paths:
      - report.html

```

## Running my tests

The beauty of all of this is that the tests can run when:

- Manually running a pipeline from the GitLab user interface.
- Pushing code to the repo.

## Conclusion
<<<<<<< HEAD

asdasdasd
=======
Using a small API was a great place to start exploring some options for having a modern automation suite running on the cloud. Besides some limitations, the setup is fairly easy and the result is quite satisfying.
The suite is now ready to be run when any changes to the API are made, so hopefully, if there’s a bug… it’ll catch’em all!
>>>>>>> e2a39778f7d68280d4d88ff98624276e32cb8b5e
